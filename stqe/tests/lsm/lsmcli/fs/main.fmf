description: Testing lsmcli FS tools.

/fs_create_success:
  description: Tests 'lsmcli fs-create' (and 'lsmcli fs-delete' to clean up).
  tier: 1
  test: fs_create.py

/fs_create_fail:
  description: Tests 'lsmcli fs-create' failures.
  tier: 2
  test: fs_create.py
  requires_setup+: [lsm/setup/create_fs]

/fs_delete_fail:
  description: Tests 'lsmcli fs-delete' failures.
  tier: 2
  test: fs_delete.py
  requires_setup+: [lsm/setup/create_fs]

/fs_clone_success:
  description: Tests 'lsmcli fs-clone'.
  tier: 1
  test: fs_clone.py
  requires_setup+: [lsm/setup/create_fs]

/fs_clone_fail:
  description: Tests 'lsmcli fs-clone' failures.
  tier: 2
  test: fs_clone.py
  requires_setup+: [lsm/setup/create_fs]

/fs_dependants_success:
  description: Tests 'lsmcli fs-dependants'.
  tier: 1
  test: fs_dependants.py
  requires_setup+:
    - lsm/setup/create_fs
    - lsm/setup/clone_fs

/fs_dependants_fail:
  description: Tests 'lsmcli fs-dependants' failures.
  tier: 2
  test: fs_dependants.py
  requires_setup+:
    - lsm/setup/create_fs
    - lsm/setup/clone_fs

/fs_dependants_rm_success:
  description: Tests 'lsmcli fs-dependants-rm'.
  tier: 1
  test: fs_dependants_rm.py
  requires_setup+: [lsm/setup/create_fs]

/fs_dependants_rm_fail:
  description: Tests 'lsmcli fs-dependants-rm' failures.
  tier: 2
  test: fs_dependants_rm.py
  requires_setup+: [lsm/setup/create_fs]

/fs_export_success:
  description: Tests 'lsmcli fs-export'.
  tier: 1
  test: fs_export.py
  requires_setup+: [lsm/setup/create_fs]

/fs_export_fail:
  description: Tests 'lsmcli fs-export' failures.
  tier: 2
  test: fs_export.py
  requires_setup+:
    - lsm/setup/create_fs
    - lsm/setup/export_fs

/fs_unexport_fail:
  description: Tests 'lsmcli fs-unexport' failures.
  tier: 2
  test: fs_unexport.py
  requires_setup+:
    - lsm/setup/create_fs
    - lsm/setup/export_fs

/fs_resize_success:
  description: Tests 'lsmcli fs-resize'.
  tier: 1
  test: fs_resize.py
  requires_setup+: [lsm/setup/create_fs]
  new_fs_size: 2G

/fs_resize_fail:
  description: Tests 'lsmcli fs-resize' failures.
  tier: 2
  test: fs_resize.py
  requires_setup+: [lsm/setup/create_fs]

/fs_snap_create_success:
  description: Tests 'lsmcli fs-snap-create'.
  tier: 1
  test: fs_snap_create.py
  requires_setup+: [lsm/setup/create_fs]

/fs_snap_create_fail:
  description: Tests 'lsmcli fs-snap-create' failures.
  tier: 2
  test: fs_snap_create.py
  requires_setup+: [lsm/setup/create_fs]

/fs_snap_delete_fail:
  description: Tests 'lsmcli fs-snap-delete' failures.
  tier: 2
  test: fs_snap_delete.py
  requires_setup+:
    - lsm/setup/create_fs
    - lsm/setup/create_fs_snap

/fs_snap_restore_success:
  description: Tests 'lsmcli fs-snap-restore'.
  tier: 1
  test: fs_snap_restore.py
  requires_setup+:
    - lsm/setup/create_fs
    - lsm/setup/create_fs_snap

/fs_snap_restore_fail:
  description: Tests 'lsmcli fs-snap-restore' failures.
  tier: 2
  test: fs_snap_restore.py
  requires_setup+:
    - lsm/setup/create_fs
    - lsm/setup/create_fs_snap
